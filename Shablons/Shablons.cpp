﻿
#include <iostream>
#include <string>

using namespace std;
								//Созданный класс к заданию 18.5 "Стек"

class Stack									
{
private:
	int Size;	 //Размер стека
	int* stack;
	int top;     //Вверхнее значение в стеке
	

public:

	Stack() //конструктор по умолчанию
	{
		Size = 3;
		stack = new int[Size];
		top = 0;
		
	}
	
	void StackSize(int NewSize) //изменить размер стека
	{
		Size = NewSize;
		top = Size;
		cout << "Изменен размер стека, теперь он " << Size << "\n\n";
	}
	
	void StackShow()  //показать стек
	{
		cout << "Размер стека: " << Size << "\n";

		if (top == 0)
			cout << "Стек пуст!!!\n\n";
		else	
			cout << "Верхний элемент стека " << top << " cо значением: " << stack[top - 1] << "\n\n";
		
	}
	
	void StackPush(int Value) //добавить элемент в стек
	{
		if (top >= Size)
		{
			int* Newstack = new int[Size + 1];
			for (int i = 0; i < Size; i++)
			{
				Newstack[i] = stack[i];
			}
			Newstack[Size] = Value;
			delete[] stack;
			stack = Newstack;
			Newstack = nullptr;
			Size++;
			top++;
		}
		else
		{
			stack[top] = Value;
			top++;
		}
		cout << "Элемент "<<top <<" со значением " <<Value <<" добавлен!\n\n";
		
	}

	int StackPop() //Достать верхний элемент из стека
	{
		if (top == 0)
		{
			cout << "Стек пуст!\n";
			return 0;
		}
		else
		{
			top--;
			return stack[top];
		}
	}
};

                      // Созданый класс и его наследники к заданию 19.5 "Наследование"
class Animal
{
public:

	virtual void Voice()
	{
		cout << "No Body-No Voice!";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Wooof!";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Miayyyy!!!";
	}
};

class Crow : public Animal
{
public:
	void Voice() override
	{
		cout << "Karrr!";
	}
};



	int main()
	{
		setlocale(LC_ALL, "ru");
									//Реализация задания 18.5 "Стек"
		cout << "Задание 18.5:\n\n";
		Stack stack;                                        
		stack.StackShow();
		stack.StackPush(56);
		stack.StackPush(67);
		stack.StackPush(876);
		stack.StackPush(567);
		stack.StackShow();
		cout << stack.StackPop() << "\n\n";
		cout << stack.StackPop() << "\n\n";
		cout << stack.StackPop() << "\n\n";
		stack.StackShow();
		stack.StackPush(456);
		stack.StackPush(23);
		stack.StackPush(345);
		stack.StackShow();
		stack.StackSize(2);
		stack.StackShow();
		stack.StackPush(34);
		stack.StackPush(567);
		stack.StackShow();
		cout << stack.StackPop() << "\n\n";
		cout << stack.StackPop() << "\n\n"; 
		cout << stack.StackPop() << "\n\n";
		cout << stack.StackPop() << "\n\n"; 
		cout << stack.StackPop() << "\n\n";
		stack.StackShow();
								 //Реализация задания 19.5 "Наследование"
		
		cout << "\n\n\nЗадание 19.5:\n\n";

		Animal* p = new Dog;
		Animal* p1 = new Cat;
		Animal* p2 = new Crow;

		Animal* MassV[3] = { p,p1,p2 };

		for (const auto& element : MassV)
		{
			element->Voice();
			cout << "\n";
		}
		


		return 0;
	}



